import React from "react";
import { useSelector } from "react-redux";
import { selectHasNewVersion } from "./redux/selectors";
import { InfoIcon } from "./icons";

export const NewVersionAlert: React.FC<{ className?: string }> = ({
  className,
}) => {
  const hasNewVersion = useSelector(selectHasNewVersion);
  if (!hasNewVersion) {
    return;
  }

  return (
    <div className={className}>
      <div className="alert alert-info">
        <span />
        <span>
          <InfoIcon /> Ny uppdatering tillgänglig.
        </span>
        <a
          href=""
          className="btn btn-sm btn-primary"
          onClick={(e) => {
            e.preventDefault();
            location.reload();
          }}
        >
          Ladda om
        </a>
      </div>
    </div>
  );
};
