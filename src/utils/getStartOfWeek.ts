export const getStartOfWeek = (date: Date): Date => {
  const returnDate = new Date(date);
  const weekDay = returnDate.getDay();

  // Set to last Monday.
  returnDate.setDate(
    returnDate.getDate() + (weekDay === 0 ? -6 : 1 + -weekDay),
  );
  returnDate.setHours(0);
  returnDate.setMinutes(0);
  returnDate.setSeconds(0);
  returnDate.setMilliseconds(0);
  return returnDate;
};
