import type React from "react";
import { CircleCheckIcon } from "./icons";

export const Timeline: React.FC<{
  points: { top?: string; bottom?: string }[];
}> = ({ points }) => {
  return (
    <ul className="timeline timeline-vertical [--timeline-col-start:auto]">
      {points.map(({ top, bottom }, index) => (
        <li key={index}>
          {index !== 0 && <hr />}
          {top && <div className="timeline-start tabular-nums">{top}</div>}
          <div className="timeline-middle">
            <CircleCheckIcon />
          </div>
          {bottom && <div className="timeline-end timeline-box">{bottom}</div>}
          {index !== points.length - 1 && <hr />}
        </li>
      ))}
    </ul>
  );
};
