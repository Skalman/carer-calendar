import type React from "react";
import { Checkbox } from "./Checkbox";
import { FormControl } from "./FormControl";
import { selectName, useAppSelector } from "./redux/selectors";
import type { EventData } from "./types";
import { dateTimeNormalizer, useForm } from "./useForm";
import type { CreateEventData } from "./redux/reducers";

interface EventFormProps {
  event: EventData | CreateEventData;
  updateEvent: <T extends keyof EventData>(
    prop: T,
    value: EventData[T],
  ) => void;
}

export const EventForm: React.FC<EventFormProps> = ({ event, updateEvent }) => {
  const authorName = useAppSelector(selectName);

  const { persistOnBlur } = useForm();

  if (!authorName) {
    return;
  }

  const {
    description,
    homeCareInformed,
    homeHealthCareInformed,
    location,
    remindingPerson,
    startDateTime,
    taxiTime,
    title,
  } = event;

  return (
    <>
      <FormControl
        label="Rubrik"
        {...persistOnBlur("title", {
          value: title,
          onPersist: (value) => {
            updateEvent("title", value || undefined);
          },
        })}
      />

      <FormControl
        label="Plats"
        placeholder="Hemma"
        {...persistOnBlur("location", {
          value: location,
          onPersist: (value) => {
            updateEvent("location", value || undefined);
          },
        })}
      />

      <FormControl
        label="Start"
        type="datetime-local"
        {...persistOnBlur("startDateTime", {
          value: new Date(startDateTime).toLocaleString("sv"),
          normalizer: dateTimeNormalizer,
          required: true,
          onPersist: (value) => {
            if (!value) {
              return;
            }

            updateEvent("startDateTime", new Date(value).toISOString());
          },
        })}
      />

      <FormControl
        label="Taxi: ankomst hem till Frank"
        type="time"
        {...persistOnBlur("taxiTime", {
          value: taxiTime,
          onPersist: (value) => {
            updateEvent("taxiTime", value);
          },
        })}
      />

      <FormControl
        label="Beskrivning"
        component="textarea"
        {...persistOnBlur("description", {
          value: description,
          onPersist: (value) => {
            updateEvent("description", value);
          },
        })}
      />

      <Checkbox
        label="Hemtjänst informerad"
        checked={homeCareInformed ?? false}
        onChange={(value) => {
          updateEvent("homeCareInformed", value || undefined);
        }}
      />

      <Checkbox
        label="Hemsjukvård informerad"
        checked={homeHealthCareInformed ?? false}
        onChange={(value) => {
          updateEvent("homeHealthCareInformed", value || undefined);
        }}
      />

      <FormControl
        label={
          <>
            Person som påminner inför avresa
            {remindingPerson === authorName && (
              <>
                {" "}
                <span className="badge badge-secondary">Du</span>
              </>
            )}
          </>
        }
        {...persistOnBlur("remindingPerson", {
          value: remindingPerson,
          onPersist: (value) => {
            updateEvent("remindingPerson", value);
          },
        })}
      />
    </>
  );
};
