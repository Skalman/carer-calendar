import React from "react";
import { Event } from "./Event";
import { useGetEventsQuery } from "./redux/reducers";
import { NewEvent } from "./NewEvent";
import {
  selectEventUuidsByTimeGroup,
  selectIsNewEventOpen,
  type TimeGroup,
} from "./redux/selectors";
import { useSelector } from "react-redux";

const timeGroupDescriptions: Record<TimeGroup, string> = {
  past: "Gamla händelser",
  thisWeek: "Den här veckan",
  nextWeek: "Nästa vecka",
  future: "I framtiden",
};

export const EventList: React.FC = () => {
  const isNewEventOpen = useSelector(selectIsNewEventOpen);
  const eventUuidsByTimeGroup = useSelector(selectEventUuidsByTimeGroup);
  const { data, error, isLoading } = useGetEventsQuery(undefined, {
    refetchOnReconnect: true,
    pollingInterval: 60 * 1000,
    skipPollingIfUnfocused: true,
    refetchOnFocus: true,
  });

  if (data) {
    return (
      <ul className="max-w-lg mx-auto">
        {isNewEventOpen && (
          <li>
            <NewEvent />
          </li>
        )}
        {[...eventUuidsByTimeGroup.entries()].map(([timeGroup, eventUuids]) => (
          <li key={timeGroup}>
            {/* -top-px because of buggy display in Firefox */}
            <div className="sticky -top-px z-[2] bg-base-200 mx-4 pb-2">
              <h3 className="text-sm uppercase muted px-4">
                {timeGroupDescriptions[timeGroup]}
              </h3>
            </div>
            <ul>
              {eventUuids.map((uuid) => (
                <li key={uuid}>
                  <Event uuid={uuid} />
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    );
  }

  if (error) {
    return (
      <div role="alert" className="alert alert-error">
        <svg
          className="stroke-current shrink-0 h-6 w-6"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
          />
        </svg>
        <span>Kunde inte ladda.</span>
      </div>
    );
  }

  if (isLoading) {
    return (
      <div className="max-w-lg mx-auto px-4">
        <div className="skeleton h-56 mb-8"></div>
        <div className="skeleton h-56 mb-8"></div>
        <div className="skeleton h-56 mb-8"></div>
      </div>
    );
  }

  return <div className="max-w-lg mx-auto px-4 alert alert-error">Error</div>;
};
