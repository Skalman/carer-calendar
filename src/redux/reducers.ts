import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import {
  combineReducers,
  createSlice,
  type PayloadAction,
} from "@reduxjs/toolkit/react";
import type { EventData } from "../types";

type UpdateEventData = {
  [P in keyof EventData]-?: {
    uuid: string;
    author: string;
    prop: P;
    value: EventData[P];
  };
}[keyof EventData];

export type CreateEventData = Omit<EventData, "uuid" | "lastUpdate">;

interface EventCacheEntry {
  event: EventData;
  state: "pending" | "ok";
}

const eventCacheSlice = createSlice({
  name: "eventCache",
  initialState: {} as Record<string, EventCacheEntry | undefined>,
  reducers: {
    cacheEvent(state, { payload }: PayloadAction<EventData>) {
      state[payload.uuid] = { event: payload, state: "ok" };
    },

    cacheEvents(state, { payload }: PayloadAction<EventData[]>) {
      for (const event of payload) {
        state[event.uuid] = { event, state: "ok" };
      }
    },

    cachePendingEventUpdate(
      state,
      { payload }: PayloadAction<UpdateEventData>,
    ) {
      const eventCacheEntry = state[payload.uuid];
      if (eventCacheEntry) {
        // @ts-expect-error The types of `prop` and `value` must match, but TS doesn't understand that.
        eventCacheEntry.event[payload.prop] = payload.value;
        eventCacheEntry.state = "pending";
      }
    },
  },
});

const { cacheEvent, cacheEvents, cachePendingEventUpdate } =
  eventCacheSlice.actions;

const appSlice = createSlice({
  name: "app",
  initialState: { hasNewVersion: false } as {
    version?: string;
    hasNewVersion: boolean;
  },
  reducers: {
    reduxInit(state, { payload }: PayloadAction<{ version?: string }>) {
      state.version = payload.version;
    },
    receivedVersion(state, { payload }: PayloadAction<string>) {
      state.hasNewVersion =
        state.version !== undefined && payload !== state.version;
    },
  },
});

export const {
  actions: { reduxInit, receivedVersion },
} = appSlice;

export const appApi = createApi({
  reducerPath: "appApi",
  baseQuery: fetchBaseQuery({ baseUrl: "api.php" }),
  endpoints: (builder) => ({
    getEvents: builder.query<EventData[], undefined>({
      query: () => ({ url: "", params: { action: "get" } }),
      async onQueryStarted(_arg, { dispatch, queryFulfilled }) {
        const { data } = await queryFulfilled;
        dispatch(cacheEvents(data));
      },
    }),

    getVersion: builder.query<string, undefined>({
      query: () => ({ url: "", params: { action: "getVersion" } }),
      async onQueryStarted(_arg, { dispatch, queryFulfilled }) {
        const { data } = await queryFulfilled;
        dispatch(receivedVersion(data));
      },
    }),

    createEvent: builder.mutation<EventData, CreateEventData>({
      query: ({
        author,
        startDateTime,
        description,
        homeCareInformed,
        homeHealthCareInformed,
        isDeleted,
        location,
        remindingPerson,
        taxiTime,
        title,
      }) => ({
        url: "",
        params: { action: "create" },
        body: JSON.stringify({
          author,
          startDateTime,
          description,
          homeCareInformed,
          homeHealthCareInformed,
          isDeleted,
          location,
          remindingPerson,
          taxiTime,
          title,
        }),
        method: "POST",
      }),
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        const { data } = await queryFulfilled;
        dispatch(cacheEvent(data));
        dispatch(eventCreated(data.uuid));
      },
    }),

    updateEvent: builder.mutation<EventData, UpdateEventData>({
      query: ({ uuid, author, prop, value }) => ({
        url: "",
        params: { action: "update", uuid },
        body: JSON.stringify({ author, prop, value }),
        method: "POST",
      }),
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        dispatch(cachePendingEventUpdate(arg));
        const { data } = await queryFulfilled;
        dispatch(cacheEvent(data));

        if (arg.prop === "startDateTime") {
          dispatch(eventStartDateTimeChanged(data.uuid));
        }

        // TODO isDeleted -> recentlyDeleted
      },
    }),
  }),
});

export const {
  useGetEventsQuery,
  useGetVersionQuery,
  useCreateEventMutation,
  useUpdateEventMutation,
} = appApi;

interface SettingsStateWrapper {
  current: SettingsState;
  original?: SettingsState;
}
export interface SettingsState {
  name?: string;
  allowEdit?: true;
  showOldEvents?: true;
  theme?: "light" | "dark";
  zoom?: number;
  disableAnimation?: true;
  disableRoundedCorners?: true;
  disableShadows?: true;
  disableScrollStick?: true;
  showPerformance?: true;
}
const settingsKeys: (keyof SettingsState)[] = [
  "name",
  "allowEdit",
  "showOldEvents",
  "theme",
  "zoom",
  "disableAnimation",
  "disableRoundedCorners",
  "disableShadows",
  "disableScrollStick",
  "showPerformance",
];
const settingsSlice = createSlice({
  name: "settings",
  initialState: { current: {} } as SettingsStateWrapper,
  reducers: {
    resetSettings(state) {
      if (state.original) {
        state.current = state.original;
        state.original = undefined;
      }
    },
    saveSettings(state, { payload }: PayloadAction<SettingsState>) {
      state.original ??= { ...state.current };
      for (const key of settingsKeys) {
        if (key in payload) {
          // @ts-expect-error TypeScript doesn't understand that it's the same key.
          state.current[key] = payload[key];
        }
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(toggleDrawer, (state) => {
      state.original = undefined;
    });
  },
});
export const {
  actions: { saveSettings, resetSettings },
} = settingsSlice;

interface UiState {
  isDrawerOpen: boolean;
  isNewEventOpen: boolean;
  scrollToEventUuid?: string;
}
const uiSlice = createSlice({
  name: "ui",
  initialState: {
    isDrawerOpen: false,
    isNewEventOpen: false,
  } as UiState,
  reducers: {
    toggleDrawer(state) {
      state.isDrawerOpen = !state.isDrawerOpen;
    },
    toggleNewEvent(state) {
      state.isNewEventOpen = !state.isNewEventOpen;
    },
    eventCreated(state, { payload }: PayloadAction<string>) {
      state.isNewEventOpen = false;
      state.scrollToEventUuid = payload;
    },
    eventStartDateTimeChanged(state, { payload }: PayloadAction<string>) {
      state.scrollToEventUuid = payload;
    },
    scrollToEventCompleted(state, { payload }: PayloadAction<string>) {
      if (state.scrollToEventUuid === payload) {
        state.scrollToEventUuid = undefined;
      }
    },
  },
});
export const {
  actions: {
    toggleDrawer,
    toggleNewEvent,
    eventCreated,
    eventStartDateTimeChanged,
    scrollToEventCompleted,
  },
} = uiSlice;

export const reducer = combineReducers({
  [appApi.reducerPath]: appApi.reducer,
  [appSlice.reducerPath]: appSlice.reducer,
  [eventCacheSlice.reducerPath]: eventCacheSlice.reducer,
  [settingsSlice.reducerPath]: settingsSlice.reducer,
  [uiSlice.reducerPath]: uiSlice.reducer,
});

export type RootState = ReturnType<typeof reducer>;
