import {
  reducer,
  saveSettings,
  type RootState,
  type SettingsState,
} from "./reducers";
import { selectSettings } from "./selectors";

type NonOptional<T> = T & Record<keyof T, unknown>;

interface SettingsLocalStorage {
  name: string | undefined;
  allowEdit: true | undefined;
  showOldEvents: true | undefined;
  theme: "light" | "dark" | undefined;
  zoom: number | undefined;
  disableAnimation: true | undefined;
  disableRoundedCorners: true | undefined;
  disableShadows: true | undefined;
  disableScrollStick: true | undefined;
}
export const settingsPersistence = {
  shouldPersist: (currentState: RootState, previousState: RootState) => {
    return selectSettings(currentState) !== selectSettings(previousState);
  },

  persist: (state: RootState) => {
    const settings = selectSettings(state);
    const storageModel: SettingsLocalStorage = {
      name: settings.name,
      allowEdit: settings.allowEdit,
      showOldEvents: settings.showOldEvents,
      theme: settings.theme,
      zoom: settings.zoom,
      disableAnimation: settings.disableAnimation,
      disableRoundedCorners: settings.disableRoundedCorners,
      disableShadows: settings.disableShadows,
      disableScrollStick: settings.disableScrollStick,
    };
    const json = JSON.stringify(storageModel);
    localStorage.setItem("settings", json);
  },

  load: (state: RootState): RootState => {
    try {
      const json = localStorage.getItem("settings");
      const parsed: unknown = json && JSON.parse(json);
      if (parsed && typeof parsed === "object") {
        const storageModel = parsed as SettingsLocalStorage;
        return reducer(
          state,
          saveSettings({
            name: storageModel.name,
            allowEdit: storageModel.allowEdit,
            showOldEvents: storageModel.showOldEvents,
            theme: storageModel.theme,
            zoom: storageModel.zoom,
            disableAnimation: storageModel.disableAnimation,
            disableRoundedCorners: storageModel.disableRoundedCorners,
            disableShadows: storageModel.disableShadows,
            disableScrollStick: storageModel.disableScrollStick,
            showPerformance: undefined,
          } satisfies NonOptional<SettingsState>),
        );
      }
    } catch {
      // Do nothing.
    }

    return state;
  },
};
