import { createListenerMiddleware } from "@reduxjs/toolkit/react";
import { type RootState, reduxInit } from "./reducers";
import { settingsPersistence } from "./persistence";
import { selectSettings } from "./selectors";

export const listenerMiddleware = createListenerMiddleware<RootState>();

// Settings persistence.
listenerMiddleware.startListening({
  predicate: (_action, currentState, previousState) => {
    return settingsPersistence.shouldPersist(currentState, previousState);
  },
  effect: (_action, listenerApi) => {
    settingsPersistence.persist(listenerApi.getState());
  },
});

// Settings that should change the root element.
listenerMiddleware.startListening({
  predicate: (action, currentState, previousState) => {
    return (
      action.type === reduxInit.type ||
      selectSettings(currentState) != selectSettings(previousState)
    );
  },
  effect: (_action, listenerApi) => {
    const {
      zoom,
      theme,
      disableAnimation,
      disableRoundedCorners,
      disableShadows,
      disableScrollStick,
    } = selectSettings(listenerApi.getState());
    const root = document.documentElement;
    const dataset = root.dataset;

    if (zoom) {
      root.style.fontSize = `${zoom}px`;
    } else {
      root.style.fontSize = "";
    }

    if (theme) {
      dataset.theme = theme;
    } else {
      delete dataset.theme;
    }

    disableAnimation ?
      root.classList.add("disable-animation")
    : root.classList.remove("disable-animation");

    disableRoundedCorners ?
      root.classList.add("disable-rounded-corners")
    : root.classList.remove("disable-rounded-corners");

    disableShadows ?
      root.classList.add("disable-shadows")
    : root.classList.remove("disable-shadows");

    disableScrollStick ?
      root.classList.add("disable-scroll-stick")
    : root.classList.remove("disable-scroll-stick");
  },
});
