type IsTruthy = <T>(x: T | undefined | null | false | 0 | "") => x is T;

export const isTruthy = Boolean as unknown as IsTruthy;
