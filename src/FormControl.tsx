import type React from "react";

interface FormControlInputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  component?: "input";
}

interface FormControlTextareaProps
  extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
  component: "textarea";
}

type FormControlProps = {
  label: React.ReactNode;
  className?: string;
  error?: boolean;
} & (FormControlInputProps | FormControlTextareaProps);

export const FormControl: React.FC<FormControlProps> = ({
  label,
  className,
  error,
  ...rest
}) => {
  const errorClass = error ? "textarea-error" : "";

  let inner;
  if (rest.component === "textarea") {
    inner = (
      <textarea
        className={`textarea textarea-bordered w-full max-w-xs ${errorClass}`}
        {...rest}
      />
    );
  } else {
    inner = (
      <input
        className={`input input-bordered w-full max-w-xs ${errorClass}`}
        {...rest}
      />
    );
  }

  return (
    <label className={`form-control w-full max-w-xs ${className ?? ""}`}>
      <div className="label">
        <span className="label-text">{label}</span>
      </div>
      {inner}
    </label>
  );
};
