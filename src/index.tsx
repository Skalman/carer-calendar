import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { App } from "./App";
import { reduxInit } from "./redux/reducers";

const rootElem = document.createElement("div");
rootElem.id = "app";
document.body.append(rootElem);

store.dispatch(
  reduxInit({
    version: document.documentElement.dataset.version,
  }),
);

createRoot(rootElem).render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
);
