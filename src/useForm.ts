import type React from "react";
import { useRef, useState } from "react";

const identityNormalizer = (value?: string) => value;
export const dateTimeNormalizer = (value?: string) =>
  value === undefined ? undefined : new Date(value).toLocaleString("sv");

export const useForm = () => {
  const [, updateState] = useState(0);
  const localValuesById = useRef<Record<string, string | undefined>>({});

  return {
    persistOnBlur: (
      id: string,
      {
        value,
        required,
        onPersist,
        normalizer = identityNormalizer,
      }: {
        value?: string;
        required?: boolean;
        onPersist: (value: string) => void;
        normalizer?: (value?: string) => string | undefined;
      },
    ): React.InputHTMLAttributes<HTMLInputElement> &
      React.TextareaHTMLAttributes<HTMLTextAreaElement> => {
      return {
        value: localValuesById.current[id] ?? value ?? "",
        required,
        onChange: (e) => {
          localValuesById.current[id] = e.target.value;
          updateState((x) => x + 1);
        },
        onKeyDown: (e) => {
          if (e.key === "Escape") {
            localValuesById.current[id] = undefined;
            updateState((x) => x + 1);
            e.currentTarget.value = value ?? "";
            e.currentTarget.blur();
          }
        },
        onBlur: (e) => {
          localValuesById.current[id] = undefined;
          const oldValue = normalizer(value);
          const newValue = normalizer(e.target.value || undefined);

          if (required && !newValue) {
            updateState((x) => x + 1);
            return;
          }

          if (oldValue !== newValue) {
            onPersist(e.target.value);
          }
        },
      };
    },
  };
};
