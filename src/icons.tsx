import type React from "react";

// From DaisyUI
export const CircleCheckIcon: React.FC = () => (
  <svg viewBox="0 0 20 20" fill="currentColor" className="inline-block w-5 h-5">
    <path
      fillRule="evenodd"
      d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
      clipRule="evenodd"
    />
  </svg>
);

export const CrossIcon: React.FC = () => (
  <svg
    className="inline-block h-5 w-5"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M6 18L18 6M6 6l12 12"
    />
  </svg>
);

export const InfoIcon: React.FC = () => (
  <svg
    fill="none"
    viewBox="0 0 24 24"
    className="inline-block stroke-current w-5 h-5"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
    ></path>
  </svg>
);

// From Bytesize
export const SettingsIcon: React.FC = () => (
  <svg
    viewBox="0 0 32 32"
    className="inline-block w-5 h-5"
    fill="none"
    stroke="currentcolor"
    strokeLinecap="round"
    strokeLinejoin="round"
    strokeWidth="2"
  >
    <path d="M13 2 L13 6 11 7 8 4 4 8 7 11 6 13 2 13 2 19 6 19 7 21 4 24 8 28 11 25 13 26 13 30 19 30 19 26 21 25 24 28 28 24 25 21 26 19 30 19 30 13 26 13 25 11 28 8 24 4 21 7 19 6 19 2 Z" />
    <circle cx="16" cy="16" r="4" />
  </svg>
);

export const PlusIcon: React.FC = () => (
  <svg
    viewBox="0 0 32 32"
    className="inline-block w-5 h-5"
    fill="none"
    stroke="currentcolor"
    strokeLinecap="round"
    strokeLinejoin="round"
    strokeWidth="2"
  >
    <path d="M16 2 L16 30 M2 16 L30 16" />
  </svg>
);
