import type React from "react";
import { EventList } from "./EventList";
import { useEffect, useId } from "react";
import { PlusIcon, SettingsIcon } from "./icons";
import { Settings } from "./Settings";
import { useDispatch, useSelector } from "react-redux";
import {
  selectAllowEdit,
  selectIsDrawerOpen,
  selectIsNewEventOpen,
} from "./redux/selectors";
import {
  toggleDrawer,
  toggleNewEvent,
  useGetVersionQuery,
} from "./redux/reducers";
import { NewVersionAlert } from "./NewVersionAlert";
import { PerformanceDetails } from "./PerformanceDetails";

export const App: React.FC = () => {
  const drawerId = useId();
  const dispatch = useDispatch();
  const isDrawerOpen = useSelector(selectIsDrawerOpen);
  const allowEdit = useSelector(selectAllowEdit);
  const isNewEventOpen = useSelector(selectIsNewEventOpen);

  useGetVersionQuery(undefined, {
    pollingInterval: 1000 * 60 * 10,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    skipPollingIfUnfocused: true,
  });

  useEffect(() => {
    if (!isDrawerOpen) {
      return;
    }

    const listener = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        dispatch(toggleDrawer());
      }
    };
    window.addEventListener("keydown", listener);
    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, [dispatch, isDrawerOpen]);

  return (
    <div className="drawer h-full overflow-auto">
      <input
        id={drawerId}
        type="checkbox"
        className="drawer-toggle"
        checked={isDrawerOpen}
        onChange={() => {
          dispatch(toggleDrawer());
        }}
      />
      <div className="drawer-content">
        <div className="bg-base-200 pb-20">
          <div className="max-w-lg mx-auto flex gap-2 p-4">
            <button
              className={`btn btn-ghost btn-sm ${isDrawerOpen ? "btn-outline" : "muted"}`}
              onClick={() => dispatch(toggleDrawer())}
            >
              <SettingsIcon />
              Inställningar
            </button>
            {allowEdit && (
              <button
                className={`btn btn-ghost btn-sm ${isNewEventOpen ? "btn-outline" : "muted"}`}
                onClick={() => dispatch(toggleNewEvent())}
              >
                <PlusIcon />
                Lägg till
              </button>
            )}
          </div>
          <NewVersionAlert className="max-w-lg mx-auto p-4" />
          <PerformanceDetails />
          <EventList />
        </div>
      </div>
      <div className="drawer-side z-[2]">
        <label
          htmlFor={drawerId}
          aria-label="close sidebar"
          className="drawer-overlay"
        ></label>
        <div className="p-4 w-80 max-w-full min-h-full bg-base-200 text-base-content">
          {isDrawerOpen && (
            <Settings onClose={() => dispatch(toggleDrawer())} />
          )}
        </div>
      </div>
    </div>
  );
};
