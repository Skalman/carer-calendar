export interface EventData {
  uuid: string;
  author: string;
  lastUpdate: string;
  startDateTime: string;
  description?: string;
  homeCareInformed?: true;
  homeHealthCareInformed?: true;
  isDeleted?: true;
  location?: string;
  remindingPerson?: string;
  taxiTime?: string;
  title?: string;
}
