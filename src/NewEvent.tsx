import type React from "react";
import { PlusIcon } from "./icons";
import { useState } from "react";
import { useCreateEventMutation, type CreateEventData } from "./redux/reducers";
import { selectName, useAppSelector } from "./redux/selectors";
import { getStartOfDay } from "./utils/getStartOfDay";
import { EventForm } from "./EventForm";

export const NewEvent: React.FC = () => {
  const authorName = useAppSelector(selectName);
  const [event, setEvent] = useState<CreateEventData>(() => {
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow = getStartOfDay(tomorrow);
    tomorrow.setHours(13);

    return {
      author: authorName ?? "",
      startDateTime: tomorrow.toISOString(),
    };
  });
  const [createEventMutation, { isLoading }] = useCreateEventMutation();

  if (!authorName) {
    return;
  }

  return (
    <form
      className="card card-compact shadow-xl mb-8 mx-4 animate-slidein"
      onSubmit={(e) => {
        e.preventDefault();
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        createEventMutation(event);
      }}
    >
      <div className="card-body">
        <h2 className="card-title flex-wrap">Ny händelse</h2>

        <EventForm
          event={event}
          updateEvent={(prop, value) => {
            setEvent({ ...event, [prop]: value });
          }}
        />

        <button type="submit" className="btn btn-primary">
          {isLoading ?
            <span className="loading loading-spinner"></span>
          : <PlusIcon />}{" "}
          Lägg till
        </button>
      </div>
    </form>
  );
};
