import type React from "react";
import { getStartOfDay } from "./utils/getStartOfDay";
import { Timeline } from "./Timeline";
import { isTruthy } from "./isTruthy";
import {
  selectAllowEdit,
  selectEvent,
  selectName,
  selectScrollToEvent,
  useAppSelector,
} from "./redux/selectors";
import { EditEventForm } from "./EditEventForm";
import { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { scrollToEventCompleted } from "./redux/reducers";

const dateFormat = new Intl.DateTimeFormat("sv", {
  weekday: "long",
  day: "numeric",
  month: "short",
});

const dateFormatWithYear = new Intl.DateTimeFormat("sv", {
  weekday: "long",
  day: "numeric",
  month: "short",
  year: "numeric",
});

const timeFormat = new Intl.DateTimeFormat("sv", { timeStyle: "short" });

const getIsTodayTomorrow = (date: Date) => {
  const formatted = dateFormat.format(date);
  if (dateFormat.format() === formatted) {
    return "today";
  }

  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  if (dateFormat.format(tomorrow) === formatted) {
    return "tomorrow";
  }
};

export const Event: React.FC<{ uuid: string }> = ({ uuid }) => {
  const dispatch = useDispatch();
  const allowEdit = useAppSelector(selectAllowEdit);
  const name = useAppSelector(selectName);
  const event = useAppSelector((x) => selectEvent(x, uuid));
  const [highlight, setHighlight] = useState(false);
  const scrollToEvent = useAppSelector((x) => selectScrollToEvent(x, uuid));
  const scrollToElement = useCallback(
    (element: Element | null) => {
      if (scrollToEvent && element) {
        const activeElement = document.activeElement;
        if (activeElement && element.contains(activeElement)) {
          element = activeElement;
        }
        element.scrollIntoView({ behavior: "smooth", block: "center" });
        dispatch(scrollToEventCompleted(uuid));
        setHighlight(true);
        setTimeout(setHighlight, 2000, false);
      }
    },
    [dispatch, scrollToEvent, uuid],
  );

  if (!event) {
    return;
  }

  const { description, location, startDateTime, taxiTime, title } = event;

  const startDateTimeDate = new Date(startDateTime);
  const todayDate = getStartOfDay(new Date());
  const isBeforeToday = startDateTimeDate < todayDate;
  const startDateTimeFormatted =
    startDateTimeDate.getFullYear() === todayDate.getFullYear() ?
      dateFormat.format(startDateTimeDate)
    : dateFormatWithYear.format(startDateTimeDate);
  const todayTomorrow = getIsTodayTomorrow(startDateTimeDate);

  return (
    <div
      data-uuid={uuid}
      className={`card card-compact shadow-xl mb-8 mx-4 ${highlight ? "ring" : ""} ${todayTomorrow ? "first:mt-4" : ""}`}
      ref={scrollToElement}
    >
      <div className="card-body">
        <div className={isBeforeToday ? "muted" : ""}>
          <h2 className="card-title flex-wrap">
            {todayTomorrow ?
              <span
                className="tooltip tooltip-open tooltip-accent"
                data-tip={{ today: "Idag", tomorrow: "Imorgon" }[todayTomorrow]}
              >
                {startDateTimeFormatted}
              </span>
            : startDateTimeFormatted}

            {location ?
              <div className="badge badge-accent whitespace-nowrap">
                {location}
              </div>
            : <div className="badge badge-outline muted">Hemma</div>}

            {name && event.remindingPerson === name && (
              <div className="badge badge-secondary badge-outline whitespace-nowrap">
                {name} påminner
              </div>
            )}
          </h2>

          <div className="flex flex-wrap gap-4 items-center">
            <Timeline
              points={[
                taxiTime && {
                  top: taxiTime,
                  bottom: "Taxi",
                },
                {
                  top: timeFormat.format(new Date(startDateTime)),
                  bottom: title ?? "Start",
                },
              ].filter(isTruthy)}
            />

            <div className="whitespace-pre-wrap">{description}</div>
          </div>
          {allowEdit && <EditEventForm uuid={uuid} className="mt-2" />}
        </div>
      </div>
    </div>
  );
};
