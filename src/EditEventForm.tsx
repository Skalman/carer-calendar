import type React from "react";
import { EventForm } from "./EventForm";
import { useUpdateEventMutation } from "./redux/reducers";
import {
  selectEvent,
  selectIsEventPending,
  selectName,
  useAppSelector,
} from "./redux/selectors";
import type { EventData } from "./types";

const dateTimeFormat = new Intl.DateTimeFormat("sv", {
  dateStyle: "long",
  timeStyle: "short",
});

interface EditEventFormProps {
  uuid: string;
  className?: string;
}

export const EditEventForm: React.FC<EditEventFormProps> = ({
  uuid,
  className,
}) => {
  const authorName = useAppSelector(selectName);
  const event = useAppSelector((x) => selectEvent(x, uuid));
  const isEventPending = useAppSelector((x) => selectIsEventPending(x, uuid));

  const [updateEventMutation] = useUpdateEventMutation();

  if (!event || !authorName) {
    return;
  }

  const updateEvent = <T extends keyof EventData>(
    prop: T,
    value: EventData[T],
  ) => {
    // @ts-expect-error TypeScript can't figure this out.
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    updateEventMutation({ author: authorName, uuid, prop, value });
  };

  return (
    <details
      className={`collapse collapse-arrow bg-base-200 transition ${isEventPending ? "ring" : ""} ${className}`}
    >
      <summary className="collapse-title min-h-0">
        <span className="[details[open]_&]:scale-125 origin-left inline-block transition">
          Ändra
        </span>
      </summary>
      <div className="collapse-content">
        <EventForm
          event={event}
          updateEvent={(prop, value) => {
            // @ts-expect-error TypeScript can't figure this out.
            // eslint-disable-next-line @typescript-eslint/no-floating-promises
            updateEventMutation({ author: authorName, uuid, prop, value });
          }}
        />

        <div className="mt-4">
          <button
            className="btn btn-error btn-sm"
            onClick={() => {
              if (confirm("Radera händelsen?")) {
                updateEvent("isDeleted", true);
              }
            }}
          >
            Radera
          </button>
        </div>
        <div className="divider" />
        <div className="text-sm">
          Senast uppdaterad {dateTimeFormat.format(new Date(event.lastUpdate))}{" "}
          av {event.author}
        </div>
      </div>
    </details>
  );
};
