import type React from "react";
import { selectSettings, useAppSelector } from "./redux/selectors";
import { Checkbox } from "./Checkbox";
import { FormControl } from "./FormControl";
import { useDispatch } from "react-redux";
import { resetSettings, saveSettings } from "./redux/reducers";
import { CrossIcon } from "./icons";
import type { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import { useState } from "react";

interface SettingsProps {
  onClose: () => void;
}

type SaveSettingsPayload =
  typeof saveSettings extends ActionCreatorWithPayload<infer T> ? T : never;

export const Settings: React.FC<SettingsProps> = ({ onClose }) => {
  const dispatch = useDispatch();
  const {
    allowEdit,
    name,
    showOldEvents,
    theme,
    zoom,
    disableAnimation,
    disableRoundedCorners,
    disableShadows,
    disableScrollStick,
    showPerformance,
  } = useAppSelector(selectSettings);

  const save = (payload: SaveSettingsPayload) => {
    dispatch(saveSettings(payload));
  };

  const [localZoom, setLocalZoom] = useState(zoom);
  const [isChangingZoom, setIsChangingZoom] = useState(false);

  return (
    <form onSubmit={onClose}>
      <div className="flex gap-4 items-center justify-between mb-2">
        <h2 className="text-xl">Inställningar</h2>
        <button
          type="button"
          className="btn btn-square btn-outline float-right"
          onClick={onClose}
          aria-label="Stäng"
        >
          <CrossIcon />
        </button>
      </div>

      <div className="mb-2">
        <p>Tema</p>
        <select
          value={theme ?? "auto"}
          onChange={(e) => {
            const theme = e.target.value;
            save({
              theme: theme === "light" || theme === "dark" ? theme : undefined,
            });
          }}
          className="select select-bordered"
        >
          <option value="auto">Automatisk</option>
          <option value="light">Ljus</option>
          <option value="dark">Mörk</option>
        </select>
      </div>

      <p className="mb-2">Zoom</p>
      <div className="relative">
        {isChangingZoom && (
          <div
            className="absolute bottom-0 animate-slidein mb-2 p-4 rounded border bg-neutral text-neutral-content w-full pointer-events-none transition-font-size shadow"
            style={{ fontSize: localZoom + "px" }}
          >
            Exempel på textstorlek
          </div>
        )}
      </div>
      <input
        type="range"
        min={10}
        max={24}
        value={localZoom ?? 16}
        onFocus={() => {
          setIsChangingZoom(true);
        }}
        onChange={(e) => {
          setLocalZoom(+e.target.value || undefined);
        }}
        onBlur={() => {
          setIsChangingZoom(false);
          save({ zoom: localZoom });
        }}
        onTouchStart={() => {
          setIsChangingZoom(true);
        }}
        onTouchEnd={() => {
          setIsChangingZoom(false);
          save({ zoom: localZoom });
        }}
        className="range"
        step={2}
      />
      <Checkbox
        label="Visa gamla händelser"
        checked={showOldEvents ?? false}
        onChange={(checked) => {
          save({ showOldEvents: checked || undefined });
        }}
      />

      <Checkbox
        label="Aktivera ändring av händelser"
        checked={allowEdit ?? false}
        onChange={(checked) => {
          save({ allowEdit: checked || undefined });
        }}
      />

      {allowEdit && (
        <FormControl
          label="Ditt namn (för att visa vem som har gjort ändringar)"
          value={name ?? ""}
          onChange={(e) => {
            save({ name: e.target.value || undefined });
          }}
          error={!name}
          required
        />
      )}

      <details className="collapse collapse-arrow">
        <summary className="collapse-title">Avancerat</summary>
        <Checkbox
          label="Avaktivera animationer"
          checked={disableAnimation ?? false}
          onChange={(checked) => {
            save({ disableAnimation: checked || undefined });
          }}
        />
        <Checkbox
          label="Avaktivera rundade hörn"
          checked={disableRoundedCorners ?? false}
          onChange={(checked) => {
            save({ disableRoundedCorners: checked || undefined });
          }}
        />
        <Checkbox
          label="Avaktivera skuggor"
          checked={disableShadows ?? false}
          onChange={(checked) => {
            save({ disableShadows: checked || undefined });
          }}
        />
        <Checkbox
          label="Avaktivera fixering av rubriker vid skroll"
          checked={disableScrollStick ?? false}
          onChange={(checked) => {
            save({ disableScrollStick: checked || undefined });
          }}
        />
        <Checkbox
          label="Visa prestandadetaljer"
          checked={showPerformance ?? false}
          onChange={(checked) => {
            save({ showPerformance: checked || undefined });
          }}
        />
      </details>

      <div className="mt-4 flex gap-4">
        <button type="submit" className="btn btn-primary">
          Spara
        </button>
        <button
          type="button"
          className="btn btn-outline"
          onClick={() => {
            dispatch(resetSettings());
            onClose();
          }}
        >
          Avbryt
        </button>
      </div>
    </form>
  );
};
