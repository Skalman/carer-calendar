#!/bin/bash

# Abort the script if a command returns an error.
set -e

if [[ "$1" != "--no-build" ]]; then
  pnpm build
fi


if [ -f .env.deploy ]; then
  # Export variables from .env.deploy file.
  set -o allexport
  source .env.deploy
  set +o allexport
fi


# Copy the content to a temporary directory.
rsync -avz --delete --quiet \
  -e ssh ./public/ ./deploy/

# Set version.
VERSION=$(date --iso-8601=seconds)
echo $VERSION > ./deploy/version.txt
sed -i "s/{{VERSION}}/$VERSION/g" ./deploy/index.html

# Generate the configuration file.
cat <<ENV > ./deploy/.env
CARER_CALENDAR_SQLITE_DATABASE=$CARER_CALENDAR_SQLITE_DATABASE
VERSION=$VERSION
ENV


# Transfer the content to the remote directory.
rsync -avz --delete -e ssh ./deploy/ "$USERNAME@$REMOTE_HOST:$REMOTE_DIR"
