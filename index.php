<?php

require 'public/api.php';

$html = file_get_contents("public/index.html");
$html = str_replace('{{VERSION}}', getVersion(), $html);
$html = preg_replace('/<html[^>]+>/', '$0<base href="public/">', $html, 1);
echo $html;
