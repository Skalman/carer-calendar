<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

$isMainScript = !debug_backtrace();

if ($isMainScript) {
  runApi();
}

function runApi()
{
  $actions = [
    'get',
    'create',
    'update',
    'getVersion',
  ];

  if (in_array(@$_GET['action'], $actions)) {
    $actionFunction = "action" . ucfirst($_GET['action']);
    $actionFunction();
  } else {
    notFound();
  }
}

function getDotEnv()
{
  $env = parse_ini_file('.env');
  $env['BASE_DIR'] = '.';

  $envFile = @$env['ENV_FILE'];
  if ($envFile) {
    $env = parse_ini_file($envFile);
    $env['BASE_DIR'] = dirname($envFile);
  }

  return $env;
}

function getDb()
{
  $env = getDotEnv();
  $baseDir = $env['BASE_DIR'];
  $dbConnectionString = $env['CARER_CALENDAR_SQLITE_DATABASE'];
  $dbConnectionString = preg_replace('/(sqlite:)\s*([^\/])/', "$1$baseDir/$2", $dbConnectionString);

  $db = new PDO($dbConnectionString);
  initDb($db);
  return $db;
}

function initDb(PDO $db)
{
  $result = $db->exec(
    'CREATE TABLE IF NOT EXISTS events (
        uuid TEXT PRIMARY KEY,
        content TEXT NOT NULL,
        startDateTime TEXT
      );
      CREATE INDEX IF NOT EXISTS idx_startDateTime ON events (startDateTime);
    '
  );
  if ($result === false) {
    internalServerError($db->errorInfo());
  }
}

function actionGet()
{
  $db = getDb();
  $monthAgo = getIsoDate('-1 month');
  $stmt = $db->prepare("SELECT content FROM events WHERE startDateTime > :monthAgo ORDER bY startDateTime");
  $stmt->execute(['monthAgo' => $monthAgo]);
  $rows = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
  $rows = array_map(
    function ($x) {
      return json_decode($x, true);
    },
    $rows
  );
  output($rows);
}

function actionCreate()
{
  $db = getDb();
  $body = @json_decode(@file_get_contents('php://input'), true);
  @$body['uuid'] && badRequest('Must not have prop uuid');
  @$body['lastUpdate'] && badRequest('Must not have prop lastUpdate');

  $startDateTime = @$body['startDateTime'] ?? badRequest('Missing prop startDateTime');
  $author = @$body['author'] ?? badRequest('Missing prop author');
  $uuid = base64_encode(random_bytes(12));

  $content = array_merge([
    'uuid' => $uuid,
    'author' => $author,
    'lastUpdate' => getIsoDate('now'),
    'startDateTime' => $startDateTime,
  ], $body);

  $stmt = $db->prepare(
    'INSERT INTO events (uuid, content, startDateTime)
    VALUES (:uuid, :content, :startDateTime);'
  );

  $stmt->execute([
    'uuid' => $uuid,
    'content' => json_encode($content),
    'startDateTime' => $startDateTime,
  ]);

  output($content);
}

function actionUpdate()
{
  $db = getDb();
  $uuid = @$_GET['uuid'] ?? badRequest('Missing param uuid');
  $body = @json_decode(@file_get_contents('php://input'), true);
  $author = @$body['author'] ?? badRequest('Missing prop author');
  $key = @$body['prop'] ?? badRequest('Missing prop prop');
  $value = @$body['value'];

  if (in_array($key, ['uuid', 'author', 'lastUpdate'])) {
    badRequest('Disallowed key');
  }

  $stmt = $db->prepare(
    'SELECT content
    FROM events
    WHERE uuid = :uuid'
  );
  $stmt->execute(['uuid' => $uuid]);
  $content = json_decode($stmt->fetchColumn(), true);
  if ($content === null) {
    notFound();
  }

  if ($value !== null) {
    $content[$key] = $value;
  } else {
    unset($content[$key]);
  }
  $content['author'] = $author;
  $content['lastUpdate'] = getIsoDate('now');

  $stmt = $db->prepare(
    'UPDATE events
    SET content = :content
    WHERE uuid = :uuid'
  );
  $result = $stmt->execute([
    'uuid' => $uuid,
    'content' => json_encode($content),
  ]);
  if ($result === false) {
    internalServerError($stmt->errorInfo());
  }
  output($content);
}

function getVersion(): string
{
  $env = getDotEnv();
  $version = @$env['VERSION'];
  if ($version === null) {
    $filesToCheck = ['dist/index.js', 'dist/index.css', 'index.html', 'api.php'];
    $latestModification = max(array_map(fn($file) => filemtime(__DIR__ . '/' . $file), $filesToCheck));
    $version = date('c', $latestModification);
  }
  return $version;
}

function actionGetVersion()
{
  output(getVersion());
}

function notFound()
{
  header('HTTP/1.1 404 Not Found');
  output('Not found');
  exit;
}

function badRequest($obj)
{
  header('HTTP/1.1 400 Bad Request');
  output($obj);
  exit;
}

function internalServerError($obj)
{
  header('HTTP/1.1 500 Internal Server Error');
  output($obj);
  exit;
}

function output($obj)
{
  header('Content-Type: application/json');
  echo json_encode($obj);
}

function getIsoDate(string $datetime)
{
  return (new DateTime($datetime, new DateTimeZone('UTC')))->format('c');
}