/* eslint-env node */
module.exports = {
  root: true,
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/strict-type-checked",
    "plugin:@typescript-eslint/stylistic-type-checked",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
  ],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  parserOptions: {
    project: true,
    tsconfigRootDir: __dirname,
  },
  overrides: [
    {
      files: ["*.cjs"],
      extends: ["plugin:@typescript-eslint/disable-type-checked"],
    },
  ],
  ignorePatterns: ["public/dist/*", "deploy/*"],
  settings: {
    react: {
      version: "detect",
    },
  },
};
